<?php

/**
 * @file
 * Admin pages.
 */

/**
 * Settings form of register validation step module.
 */
function register_validation_step_settings_form($form, &$form_state) {

  $form['register_validation_step_strict_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Strict Mode'),
    '#options' => array('1'=>t('On'), '0'=>t('Off')),
    '#default_value' => variable_get('register_validation_step_strict_mode', 0),
    '#description' => t('Will not allow user to refresh the page or go to another page and came again with the same validation.'),
  );

  return system_settings_form($form);
}
