<?php

/**
 * @file
 * Needful pages.
 */

/**
 * Register validation step form.
 */
function register_validation_step_form($form, &$form_state) {
  // Remove session data if available.
  if( isset($_SESSION['register_validation_step']) )
    unset($_SESSION['register_validation_step']);

  // This is the default button to handle the submit to next step.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
    '#weight' => 1000,
  );
  return $form;
}

/**
 * Register validation step form submit.
 * You can alter this form submit and validation to do your validations.
 */
function register_validation_step_form_submit($form, &$form_state) {
  $_SESSION['register_validation_step']['pass'] = TRUE;
  $_SESSION['register_validation_step']['form_state'] = $form_state;
  $form_state['redirect'] = 'user/register';
  drupal_redirect_form($form_state);
}
